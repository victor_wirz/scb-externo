package com.victorwirz;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import kong.unirest.json.JSONObject;

class CiclistaTest {

	private Ciclista ciclista;
	
	@BeforeEach                                         
    public void setUp() throws Exception {
        ciclista  = new Ciclista();
    }
	
	//Testa se o nome do ciclista retornado é ciclista3
	@Test
	public void testFuncaoBuscarCiclistaIntegracao() {
		ciclista = Ciclista.buscarCiclistaIntegracao("fcf9c76a-a99f-42e7-ba0d-590857b87594");
		assertEquals("ciclista3", ciclista.getNome());
	}
	
	//Testa se a resposta retornada nao esta vazia
	@Test
	public void testMicrosservicoCiclistaNotNull() throws UnirestException{
		String id = "fcf9c76a-a99f-42e7-ba0d-590857b87594";
		HttpResponse<JsonNode> resposta = Unirest.get("https://pm-2022-1.herokuapp.com/ciclista/{idCiclista}")
				  .header("accept", "application/json")
				  .routeParam("idCiclista", id)
			      .asJson();	

		// System.out.println(resposta.getBody());
		assertNotNull(resposta.getBody());
	}
	
	//Testa se o status do servico e 200
	@Test
	public void testMicrosservicoCiclistaStatus() throws UnirestException{
		String id = "fcf9c76a-a99f-42e7-ba0d-590857b87594";
		HttpResponse<JsonNode> resposta = Unirest.get("https://pm-2022-1.herokuapp.com/ciclista/{idCiclista}")
				  .header("accept", "application/json")
			      .routeParam("idCiclista", id)
			      .asJson();	
		
		assertEquals(200, resposta.getStatus());
	}
	
}
