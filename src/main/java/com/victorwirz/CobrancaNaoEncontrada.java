package com.victorwirz;

public class CobrancaNaoEncontrada extends Exception {
	public CobrancaNaoEncontrada(String mensagem) {
		super(mensagem);
	}

}
