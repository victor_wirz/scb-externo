package com.victorwirz;

public class TransacaoNaoAutorizada extends Exception {
	public TransacaoNaoAutorizada(String mensagem) {
		super(mensagem);
	}

}
