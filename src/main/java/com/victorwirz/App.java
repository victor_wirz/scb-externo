package com.victorwirz;
import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import cieloecommerce.sdk.ecommerce.request.CieloRequestException;
public class App {

    public static void main(String[] args) {
		Javalin app = Javalin.create(configuracao -> {
            configuracao.registerPlugin(configurarOpenApiPlugin());
            configuracao.defaultContentType = "application/json";
        }).start(getHerokuAssignedPort());

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            app.stop();
        }));
        
        app.events(event -> {
            event.serverStopping(() -> { });
            event.serverStopped(() -> { });
        });

        app.get("/", ctx -> ctx.result("Hello World"));

		app.post("/enviarEmail", requisicao -> {
			try {
				// SMTP gmail fora do ar (Could not connect to SMTP host: smtp.gmail.com)
				// retornando mensagem dummy confirmando recebimento

				Email e_mail = requisicao.bodyAsClass(Email.class);
				//Email.enviarEmail(e_mail);
			    
				requisicao.json(e_mail);
			    requisicao.status(200);

			} /* catch (AddressException excessaoEndereco) {
				Erro erro = new Erro("404", "Email com formato invalido.");
				requisicao.status(Integer.parseInt(erro.getCodigo()));
				requisicao.json(erro);
			} catch (MessagingException excessaoEmail) {
				Erro erro = new Erro("500", "Email com formato invalido.");
				System.out.print(excessaoEmail);
				requisicao.status(Integer.parseInt(erro.getCodigo()));
				requisicao.json(erro);
			} */
			finally {

			}
           
            
        });

		app.post("/cobranca", ctx -> {
			try {
				Cobranca cobranca = ctx.bodyAsClass(Cobranca.class);
				cobranca.realizarCobranca();
				ctx.status(200);				
				ctx.json(cobranca);
			}/*catch(CieloRequestException erroRequest) {              // Erro de requisicao a Cielo
				ctx.json(erroRequest.getError().getMessage());
			}catch(IOException erroIO) {                            //  Algum dado está errado
				ctx.json(erroIO.getMessage());
			}
			catch(CiclistaNaoEncontrado ciclistaNaoEncontado) {
				Erro erro = new Erro("422", "Dados Invalidos.");
				ctx.status(Integer.parseInt(erro.getCodigo()));
				ctx.json(erro);
			}
			catch(CartaoNaoEncontrado cartaoNaoEncontado) {
				Erro erro = new Erro("422", "Dados Invalidos.");
				ctx.status(Integer.parseInt(erro.getCodigo()));
				ctx.json(erro);
			}
			catch(TransacaoNaoAutorizada transacaoNaoAutorizada) {
				Erro erro = new Erro("422", "Transacao Nao Autorizada");
				ctx.status(Integer.parseInt(erro.getCodigo()));
				ctx.json(erro);
			}*/
			catch(Exception excessao) {
				System.out.println(excessao);
				Erro erro = new Erro("422", excessao.getMessage());
				ctx.status(Integer.parseInt(erro.getCodigo()));
				ctx.json(erro);		
			}
        });

		app.post("/filaCobranca", requisicao -> {
			try {
				Cobranca cobranca = requisicao.bodyAsClass(Cobranca.class);
				FilaCobranca.inserir(cobranca);
			}catch(Exception excessao) {
				Erro erro = new Erro("422", "Dados Invalios.");
				requisicao.status(Integer.parseInt(erro.getCodigo()));
				requisicao.json(erro);
			}
			
        });

		app.get("/cobranca/{idCobranca}", requisicao -> {
			try {
				Cobranca c = requisicao.bodyAsClass(Cobranca.class);
				Cobranca cobranca = Cobranca.consultarCobranca(c.getId());
				requisicao.status(200);
				requisicao.json(cobranca);
				
			}catch(CobrancaNaoEncontrada cobrancaNaoEncontrada) {
				Erro erro = new Erro("404", cobrancaNaoEncontrada.getMessage());
				requisicao.status(Integer.parseInt(erro.getCodigo()));
				requisicao.json(erro);
			}
        });

		app.post("/validaCartaoDeCredito", requisicao -> {
			CartaoDeCredito cc = requisicao.bodyAsClass(CartaoDeCredito.class);
			if(CartaoDeCredito.validaCartao(cc)) {
				requisicao.status(200);
			}
			else {
				Erro erro = new Erro("422", "Cartao invalido");
				requisicao.status(Integer.parseInt(erro.getCodigo()));
				requisicao.json(erro);
			}
     });
	}

	
    private static OpenApiPlugin configurarOpenApiPlugin() {
        Info info = new Info().version("1.0").description("Bicicleta API");
        OpenApiOptions opcoes = new OpenApiOptions(info)
            .activateAnnotationScanningFor("io.javalin.example.java")
            .path("/swagger-docs") 
            .swagger(new SwaggerOptions("/swagger-ui"))
            .reDoc(new ReDocOptions("/redoc")) 
            .defaultDocumentation(doc -> {
                doc.json("500", Erro.class);
                doc.json("503", Erro.class);
        });

        return new OpenApiPlugin(opcoes);
    }

    private static int getHerokuAssignedPort() {
        String herokuPort = System.getenv("PORT");
        if (herokuPort != null) {
          return Integer.parseInt(herokuPort);
        }
        return 7070;
    }

}