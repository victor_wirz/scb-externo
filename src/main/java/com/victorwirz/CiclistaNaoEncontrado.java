package com.victorwirz;

public class CiclistaNaoEncontrado extends Exception {
	public CiclistaNaoEncontrado(String mensagem) {
		super(mensagem);
	}
}
