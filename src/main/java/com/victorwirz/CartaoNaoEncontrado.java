package com.victorwirz;

public class CartaoNaoEncontrado extends Exception {
	public CartaoNaoEncontrado(String mensagem) {
		super(mensagem);
	}

}
