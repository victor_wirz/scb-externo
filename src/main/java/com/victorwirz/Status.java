package com.victorwirz;

public enum Status {
	PENDENTE, PAGA, FALHA, CANCELADA, OCUPADA
}
